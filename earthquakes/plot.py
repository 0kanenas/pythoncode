from numpy import exp
import matplotlib.pyplot as plt
from pprint import pprint
from mpl_toolkits.basemap import Basemap

data_file = 'event_output-last_2_days.txt'

with open(data_file, 'r') as data_to_parse:
    read_lines = data_to_parse.readlines()


earthquakes = []
for read_line in read_lines:
    read_line.strip()
    words = read_line.split(' ')
    new_word = []
    word_index = 0
    for word in words:
        if word != '':
            new_word.append(word)
            word_index += 1
        if word_index > 9:
            break
    #   new_word = new_word[0:10]
    earthquakes.append(new_word)

earthquakes.pop(0)

'''
#   map
m = Basemap(projection='mill', llcrnrlat=34, urcrnrlat=44,
            llcrnrlon=18, urcrnrlon=30, resolution='f')
m.drawcoastlines()
m.drawcountries()
m.drawmapboundary(fill_color='aqua')
m.fillcontinents(color='coral', lake_color='aqua')
plt.show()
'''


def plot_points():
    counter_grey, counter_lightcoral, counter_lightskyblue, counter_yellow, counter_orange, counter_red = 0, 0, 0, 0, 0, 0
    plt.figure(1)
    plt.title('Earthquakes')
    for earthquake in earthquakes:
        mag = float(earthquake[-1])
        dep = float(earthquake[-2])
        lon = float(earthquake[-3])
        lat = float(earthquake[-4])
        if mag < 1.9:
            col = 'grey'
            if counter_grey == 0:
                plt.scatter(lon, lat, c=col, label='< 1.9')
                counter_grey += 1
        elif mag < 3.9:
            col = 'lightcoral'
            if counter_lightcoral == 0:
                plt.scatter(lon, lat, c=col, label='< 3.9')
                counter_lightcoral += 1
        elif mag < 4.9:
            col = 'lightskyblue'
            if counter_lightskyblue == 0:
                plt.scatter(lon, lat, c=col, label='< 4.9')
                counter_lightskyblue += 1
        elif mag < 5.9:
            col = 'yellow'
            if counter_yellow == 0:
                plt.scatter(lon, lat, c=col, label='< 5.9')
                counter_yellow += 1
        elif mag < 6.9:
            col = 'orange'
            if counter_orange == 0:
                plt.scatter(lon, lat, c=col, label='< 6.9')
                counter_yellow += 1
        else:
            col = 'red'
            if counter_red == 0:
                plt.scatter(lon, lat, c=col, label='6.9 <')
                counter_red += 1
        '''
        x, y = m(lon, lat)
        m.plot(x, y, color=col)
        '''
        plt.scatter(lon, lat, c=col)
    plt.legend()
    plt.show()

#   pprint(earthquakes, width=200)
#   print(earthquakes[0])


index = 0
for item in earthquakes[0]:
    print(index, item)
    index += 1

plot_points()

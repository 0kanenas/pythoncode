# Class usage example

# Person is a custom datatype created using a class


class Person:
    def __init__(self, name, age):
        self._name = name
        self._age = age

    # properties for retrieving and setting the _name attribute
    @property
    def Name(self):
        return self._name

    @Name.setter
    def Name(self, value):
        self._name = value

    @property
    def Age(self):
        return self._age

    @Age.setter
    def Age(self, value):
        self._age = value

    def __repr__(self):
        return f"Name:{self._name}\tAge:{self._age}"


# The People class creates a list of Persons
class People():
    def __init__(self):
        self._persons = []

    def addPerson(self, person):
        self._persons.append(person)

    @property
    def List(self):
        return self._persons

    def findPerson(self, name):
        for person in self._persons:
            if person.Name.lower() == name.lower():
                return person
        print("\n")

    def removePerson(self, name):
        person = self.findPerson(name)
        if person is not None:
            return self._persons.remove(person)

    def dump(self):
        for p in self._persons:
            print(p)


if __name__ == "__main__":
    peoplelist = People()  # Instantiate the object
    print("\nLoading People")
    # Instantiate each person and add that person to the list
    peoplelist.addPerson(Person("John", 20))
    peoplelist.addPerson(Person("Paul", 25))
    peoplelist.addPerson(Person("Ringo", 30))
    peoplelist.addPerson(Person("George", 35))

    print("\nDumping People List")
    peoplelist.dump()

    # Find one of the people in the list
    print("\nFinding George")
    george = peoplelist.findPerson("George")
    print(george)

    # Delete the person we found in the list
    print("\nDeleting George")
    peoplelist.removePerson("George")
    peoplelist.dump()

    print("\nFinished...")

"""
A script that plots earthquakes onto a map.
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import read_data
import concurrent.futures
import time
import matplotlib.patches as mpatches


def color_magnitude(magnitude):
    if magnitude < 1.9:
        return 'tab:gray'
    elif magnitude < 3.9:
        return 'tab:cyan'
    elif magnitude < 4.9:
        return 'tab:blue'
    elif magnitude < 5.9:
        return 'tab:olive'
    elif magnitude < 6.9:
        return 'tab:orange'
    else:
        return 'tab:red'


def alpha_magnitude(magnitude):
    return 1/(1+np.exp(-round(magnitude)+np.pi))


def markersize_magnitude(magnitude):
    return 15*alpha_magnitude(magnitude)


latts = [
    read_data.earthquakelist.Coordinates(earthquake_index)[0]
    for earthquake_index in range(1, read_data.earthquakelist.NumberofEarthquakes)
]

longs = [
    read_data.earthquakelist.Coordinates(earthquake_index)[1]
    for earthquake_index in range(1, read_data.earthquakelist.NumberofEarthquakes)
]

m = Basemap(
    projection='mill',
    llcrnrlat=round(min(latts))-2,
    urcrnrlat=round(max(latts))+2,
    llcrnrlon=round(min(longs))-2,
    urcrnrlon=round(max(longs))+2,
    resolution='f'
)

m.drawcoastlines()
m.fillcontinents()

plt.tight_layout()
plt.grid()


def plot_event(earthquake_index):
    #   plt.title(f"Earthquake number: {earthquake_index:04d}")
    ex, ey = m(read_data.earthquakelist.Coordinates(earthquake_index)[1],
               read_data.earthquakelist.Coordinates(earthquake_index)[0])
    return m.plot(
        ex, ey,
        marker='o',
        color=color_magnitude(
            read_data.earthquakelist.Characteristics(earthquake_index)[-1]),
        alpha=alpha_magnitude(
            read_data.earthquakelist.Characteristics(earthquake_index)[-1]),
        markersize=markersize_magnitude(
            read_data.earthquakelist.Characteristics(earthquake_index)[-1])
    )
    #   plt.savefig(f"images/{earthquake_index:04d}.png", dpi=500)


portion = 1
events_to_plot = list(
    range(1, int(read_data.earthquakelist.NumberofEarthquakes/portion)+1))


fig = plt.gcf()

t1 = time.perf_counter()
with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(plot_event, events_to_plot)
plt.tight_layout()
legends = [
    mpatches.Patch(color=color_magnitude(1), label='< 1.9'),
    mpatches.Patch(color=color_magnitude(3), label='< 3.9'),
    mpatches.Patch(color=color_magnitude(4), label='< 4.9'),
    mpatches.Patch(color=color_magnitude(5), label='< 5.9'),
    mpatches.Patch(color=color_magnitude(6), label='< 6.9'),
    mpatches.Patch(color=color_magnitude(7), label='6.9 <'),
]
plt.legend(handles=legends)
plt.savefig('fig1.png', dpi=400)
t2 = time.perf_counter()

print(f'{events_to_plot[-1]} events rendered in {round(t2-t1)} seconds')

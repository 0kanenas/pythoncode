"""
A script that parses a .txt file with earthquake data.
"""


class Earthquake:
    def __init__(self, data):
        self._date = data[0:3]
        self._time = data[3:6]
        self._coordinates = data[6:8]
        self._characteristics = data[8:]

    #   Date property
    @property
    def Date(self):
        return self._date

    @Date.setter
    def Date(self, value):
        self._date = value

    #   Time property
    @property
    def Time(self):
        return self._time

    @Time.setter
    def Time(self, value):
        self._time = value

    #   Coordinates property
    @property
    def Coordinates(self):
        return self._coordinates

    @Coordinates.setter
    def Coordinates(self, value):
        self._coordinates = value

    #   Characteristics property
    @property
    def Characteristics(self):
        return self._characteristics

    @Characteristics.setter
    def Characteristics(self, value):
        self._characteristics = value

    def PrintEvent(self):
        print(f"{self.Date} {self.Time} {self.Coordinates} {self.Characteristics}")


class Earthquakes:
    def __init__(self):
        self._earthquakes = []

    def addEarthquake(self, earthquake):
        self._earthquakes.append(earthquake)

    @property
    def List(self):
        return self._earthquakes

    def Index(self, value):
        return self._earthquakes[value]

    def Coordinates(self, value):
        return self._earthquakes[value].Coordinates

    def Characteristics(self, value):
        return self._earthquakes[value].Characteristics

    def printEarthquakes(self):
        for earthquake in self._earthquakes:
            earthquake.PrintEvent()

    @property
    def NumberofEarthquakes(self):
        return len(self._earthquakes)


def formatted_data(item, item_index):
    if item_index < 5:
        return(int(item))
    else:
        return(float(item))


data_file = 'event_output-last_2_days.txt'
#   The file to parse data from
with open(data_file, 'r') as data_to_parse:
    #   Parse the data
    read_lines = data_to_parse.readlines()

#   Make a formatted list from the data text file
earthquakelist = Earthquakes()
for read_line in read_lines:
    sentence = read_line.strip()
    words = sentence.split(' ')
    new_sentence = []
    word_index = 0
    for word in words:
        if word != '':
            data = word
            try:
                new_sentence.append(formatted_data(word, word_index))
            except ValueError:
                new_sentence.append(word)
            word_index += 1
        if word_index > 9:
            break
    earthquakelist.addEarthquake(Earthquake(new_sentence))

#   For the challenge #100daysofcode, by 0kanenas.
#   First script.

#   Import modules
import time
import datetime
import os


#   Function to get current hour and minute
def now_time():
    now_hour = datetime.datetime.now().hour
    now_minute = datetime.datetime.now().minute
    return [now_hour, now_minute]


#   Function to get alarm hour and minute
def alarm_time():
    alarm_hour = int(input('Enter the hour the alarm should go off:\t\t'))
    alarm_minute = int(input('Enter the minute the alarm should go off:\t'))
    return [alarm_hour, alarm_minute]


#   Print to screen alarm time
alarm = alarm_time()
print(f'\nAlarm time\t-\t{alarm[0]:02d}:{alarm[1]:02d}')

#   Start alarm functionality and print to screen current time
while True:
    now = now_time()
    print(f'\rCurrent time\t-\t{now[0]:02d}:{now[1]:02d}', end='', flush=True)
    if alarm == now:
        break
    else:
        time.sleep(1)
print()

#   Open audio file
try:
    os.system('vlc alarmy.flac')
except Exception:
    print(f'Error, {Exception}')

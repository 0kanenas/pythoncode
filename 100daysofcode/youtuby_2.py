import os


#   Read contents from file
def read_links_from_file():
    file_to_read = 'contents.txt'
    with open(file_to_read, 'r') as file:
        lines = file.readlines()
    links = [line[:-1] for line in lines]
    return links


#   Get each content format to file
def save_contents_formats_to_file(link):
    formats_to_file = 'temp_formats.txt'
    command = f'youtube-dl {link} --list-formats >> {formats_to_file}'
    os.system(command)
    return formats_to_file


#   Read content format from file
def read_format_from_file(formats_to_file):
    file_to_read = formats_to_file
    with open(file_to_read, 'r') as file:
        lines = file.readlines()
    lines = [line[:-1] for line in lines[5:]]
    content_formats = lines
    return content_formats


#   Delete formats' file
def delete_formats_file(formats_to_file):
    command = f'rm {formats_to_file}'
    os.system(command)


#   Get video formats
def get_video_format(format_lines):
    try:
        video_format_lines = []
        for format_line in format_lines:
            if 'audio only' not in format_line:
                line = format_line.split()
                if line[3][-1] == 'p':
                    video_format_lines.append(line)
        video_codes, video_bitrates = [], []
        for video_format_line in video_format_lines:
            video_code = int(video_format_line[0])
            video_codes.append(video_code)
            video_bitrate = int(video_format_line[4][:-1])
            video_bitrates.append(video_bitrate)
        max_video_bitrate = max(video_bitrates)
        index, length = 0, len(video_format_line)
        while index <= length:
            if video_bitrates[index] == max_video_bitrate:
                video_format_code = video_codes[index]
            index += 1
        return video_format_code
    except Exception:
        print('An error occured while getting video format:', Exception)


#   Get audio formats
def get_audio_format(format_lines):
    try:
        audio_format_lines = []
        for format_line in format_lines:
            if 'audio only' in format_line:
                if 'fps' not in format_line:
                    line = format_line.split()
                    audio_format_lines.append(line)
        audio_codes, audio_bitrates = [], []
        for audio_format_line in audio_format_lines:
            audio_code = int(audio_format_line[0])
            audio_codes.append(audio_code)
            audio_bitrate = float(audio_format_line[6][:-1])
            audio_bitrates.append(audio_bitrate)
        max_audio_bitrate = max(audio_bitrates)
        index, length = 0, len(audio_format_lines)
        while index < length:
            if audio_bitrates[index] == max_audio_bitrate:
                audio_format_code = audio_codes[index]
            index += 1
        return audio_format_code
    except Exception:
        print('An error occured while getting audio format:', Exception)


#   main function
def main():
    links = read_links_from_file()
    for link in links:
        print(f'Video {links.index(link)+1} of {len(links)}')
        temp_file = save_contents_formats_to_file(link)
        content_formats = read_format_from_file(temp_file)
        delete_formats_file(temp_file)
        video_code = get_video_format(content_formats)
        audio_code = get_audio_format(content_formats)
        print(f'{video_code}+{audio_code}')


#   Run main function
main()

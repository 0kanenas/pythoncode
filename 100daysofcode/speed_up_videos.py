#   Import modules
import os


#   A function to print one item of a list per line
def print_list(some_list):
    for some_item in some_list:
        print(some_item)


#   Ask and check for user input
def get_user_choice():
    while True:
        try:
            user_choice = int(input('\nUser choice:\t'))
            if user_choice in [1, 2, 0]:
                break
        except:
            print('Wrong input.')
    return user_choice


#   Speed up video
def speed_up(video, video_index, factor):
    video_title = video.split('.')[0]
    video_extension = video.split('.')[1]
    final_video = f'{video_title}_speed-{factor}_temp.{video_extension}'
    #   ffmpeg -i input.mkv -filter:v "setpts=0.5*PTS" output.mkv
    #   ffmpeg -i input.mov -vf "setpts=(PTS-STARTPTS)/30" -crf 18 output.mov
    command = f'ffmpeg -i "{video}" -vf "setpts=(PTS-STARTPTS)/{factor}" "{final_video}"'
    os.system(command)


files = os.listdir()
files.sort()
videos = [file for file in files if 'mp4' in file]

#   Ask for factor
if len(videos) != 0:
    print('How much faster should the video be sped up?')
    while True:
        try:
            factor = int(input('Enter a positive integer number:\t'))
            if factor > 0:
                break
            else:
                print('Wrong input.')
        except:
            continue

#   Loop and do
for video in videos:
    print(f'\nSpeeding up "{video}".\nContinue?\n1.\tYes\n2.\tNo\n0.\tExit')
    user_choice = get_user_choice()
    if user_choice != 2:
        if user_choice == 1:
            speed_up(video, videos.index(video), factor)
        else:
            break

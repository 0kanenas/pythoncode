#   Import modules
from trello import TrelloClient
from pprint import pprint


#   Open the file where your credentials are stored and read them
with open('cred.txt', 'r') as cred:
    lines = cred.readlines()

#   Set them for use by the API
key = str(lines[0][:-1])
secret = str(lines[-1])
client = TrelloClient(api_key=key, api_secret=secret)

#   A function that prints each element of a list
def read_list(list):
    index = 0
    while index < len(list):
        print(f'{list[index]}\t{type(list[index])}')
        index += 1
    print()

#   Get all boards
all_boards = client.list_boards(board_filter='all')                                 #   These lists have classes for items
#   Get closed boards
closed_boards = client.list_boards(board_filter='closed')                           #   These lists have classes for items
#   Create open boards
open_boards = [board for board in all_boards if board not in closed_boards]         #   These lists have classes for items



#   DO STUFF

#       Print for each board its index, value and type from the open_boards list
#   for board in open_boards:
#       print(f'{open_boards.index(board)}\n{board}\n{type(board)}\n')


#       Set to variables the boards you wish to use
#   gtd_board_class = open_boards[0]
#   weekly_plan_board_class = open_boards[1]

#       Set and print the trello lists from the gtd board
#   gtd_board_lists = gtd_board_class.all_lists()
#   read_list(gtd_board_lists)

#       Set and print the trello lists from the weekly plan board
#   weekly_plan_board_lists = gtd_board_class.all_lists()
#   read_list(weekly_plan_board_class)

#       Get the cards in the gtd board
#   gtd_board_cards = gtd_board_class.all_cards()

#       Get the cards in the weekly plan board
#   weekly_plan_board_cards = weekly_plan_board_class.all_cards()

#       Get the specific card you want to mess with
#   challenge = gtd_board_cards[1]


#       To add a checklist of 100 items in a card in a list in a board in trello,
#       you have to use something like the following.
#   challenge.add_checklist('Days', (f'Day {i:03d}' for i in range(1, 101)))

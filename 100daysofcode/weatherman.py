import wolframalpha


app_id = 'HTVG6T-6U98UXVEL2'
client = wolframalpha.Client(app_id)

rest_of_today = "What's the weather forecast in Heraklion, Greece for the rest of today?"
rest_of_today = client.query(rest_of_today)
print('\nThe weather forecast for the rest of today is:')
#   reply = rest_of_today.results
reply = next(rest_of_today.results).text
print(reply)



"""
while True:
    print('Get the weather for:\n1.\tthe rest of today\n2.\ttomorrow\n3.\tthe next five days\n0.\tExit\n')
    user_choice_00 = int(input(('User choice:\t')))
    if user_choice_00 in range(0, 4):
        break

if user_choice_00 != 0:
    if user_choice_00 == 1:
        #   Get rest oftoday's weather forecast
        rest_of_today = "What's the weather forecast in Heraklion, Greece for the rest of the day?"
        rest_of_today = client.query(rest_of_today)
        print('\nThe weather forecast for the rest of today is:')
        reply = next(rest_of_today.results).text
    elif user_choice_00 == 2:
        #   Get tomorrow's weather forecast
        tomorrow = "What's the weather forecast in Heraklion, Greece for tomorrow?"
        tomorrow = client.query(tomorrow)
        print('\nThe weather forecast for tomorrow is:')
        reply = next(tomorrow.results).text
    else:
        #   Get five days' weather forecast
        five_days = "What's the weather forecast in Heraklion, Greece for the next five days?"
        five_days = client.query(five_days)
        print('\nThe weather forecast for the following five days is:')
        reply = next(five_days.results).text
    print(reply)
else:
    print('Exiting')
"""
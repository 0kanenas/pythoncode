#   A script that downloads content from YouTube

#   Import modules
import os
from pprint import pprint


os.chdir('/home/kanenas/Documents/GitHub/my100daysofcode/youtube-dl/Videos')

def get_user_input(some_list):
    while True:
        user_choice = int(input('Your choice:\t'))
        if user_choice in some_list:
            break
        else:
            print('Invalid input')
    return user_choice


def read_links():
    with open('content.txt', 'r') as file:
        lines = file.readlines()
    links = [line[:-1] for line in lines]
    return links


def get_content_formats(link):
    print(f'youtube-dl --list-format {link}')
    os.system(f'youtube-dl --list-format {link}')
    command_get_formats = f'youtube-dl --list-format {link} > temp_format.txt'
    os.system(command_get_formats)
    with open('temp_format.txt', 'r') as format_file:
        lines = format_file.readlines()
    command_remove_temp_file = 'rm temp_format.txt'
    os.system(command_remove_temp_file)
    lines = [line[:-1].split() for line in lines[4:]]
    return lines


def get_format(formats_list, type_of_format):
    format_list, format_codes, format_sizes = [], [], []
    if type_of_format == 'video':
        for format_item in formats_list:
            if format_item[-3] == type_of_format and 'only' in format_item[-2]:
                format_code = int(format_item[0])
                format_size = float(format_item[-1][:-3])
                format_codes.append(format_code)
                format_sizes.append(format_size)
                format_list.append([format_code, format_size])
    elif type_of_format == 'audio':
        for format_item in formats_list:
            if format_item[2] == type_of_format and 'only' in format_item[3]:
                if 'DASH' in format_item[:]:
                    format_code = int(format_item[0])
                    format_size = float(format_item[-1][:-3])
                    format_codes.append(format_code)
                    format_sizes.append(format_size)
                    format_list.append([format_code, format_size])
    format_item_index = 0
    format_list_len = len(format_list)
    while format_item_index < format_list_len:
        if max(format_sizes) in format_list[format_item_index]:
            format_code = format_list[format_item_index][0]
        format_item_index += 1
    return format_code


def get_formats(link):
    formats_list = get_content_formats(link)
    video_format = get_format(formats_list, 'video')
    audio_format = get_format(formats_list, 'audio')
    formats = f'{video_format}+{audio_format}'
    print(formats)
    return formats


def type_of_content():
    option_01, option_02, option_03, option_00 = 'Download Videos', 'Download Music', 'Download Podcasts', 'Exit'
    print(
        f'Choose from the options below:\n\t1.\t{option_01}\n\t2.\t{option_02}\n\t3.\t{option_03}\n\t0.\t{option_00}')
    user_input = get_user_input([1, 2, 3, 0])
    if user_input != 0:
        type_of_content = 'audio'
        if user_input == 1:
            type_of_content = 'video'
        links = read_links()
        for link in links:
            try:
                download(link, type_of_content)
            except:
                print(f'Some error occured while downloading: {link}')


def download_content(link, formats, type_of_content):
    command = f'youtube-dl {link} --format {formats} --output \'%(uploader)s - %(title)s.%(ext)s\''
    if type_of_content == 'video':
        command = f'{command} --merge-output-format mkv '
    elif type_of_content == 'audio':
        command = f'{command} --extract-audio --audio-format flac --audio-quality 0'
    command = f'{command} --console-title --geo-bypass --continue --no-overwrites'
    os.system(command)


def download(link, type_of_content):
    formats = get_formats(link)
    download_content(link, formats, type_of_content)


def intro():
    option_01, option_00 = 'Download Content', 'Exit'
    print(
        f'Choose from the options below:\n\t1.\t{option_01}\n\t0.\t{option_00}')
    user_input_00 = get_user_input([1, 0])
    if user_input_00 != 0:
        type_of_content()


intro()

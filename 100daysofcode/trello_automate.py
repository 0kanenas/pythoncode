#   Import modules
from trello_list import *
import datetime
import time


#   Function to print trello information of some class
def print_information(some_class):
    print(f'{some_class}\t{type(some_class)}\n')


#   Function to check if a certain Trello list exists in the current wrking board
def check_for_trello_list(some_trello_list, list_of_trello_lists):
    if some_trello_list in str(list_of_trello_lists):
        #   print(f'{some_trello_list} exists.')
        return True
    else:
        #   print(f'{some_trello_list} does not exist.')
        return False




#   Get current day
current_datetime = datetime.datetime.now()
current_today = current_datetime.strftime("%A")

#   DO STUFF

#   Print for each board its index, value and type from the open_boards list
#   for board in open_boards:
#       print(f'{open_boards.index(board)}\n{board}\n{type(board)}\n')

working_board = open_boards[1]
print_information(working_board)

"""
#   Get and print all lists
working_board_all_lists = working_board.all_lists()
read_list(working_board_all_lists)
#   Get and print closed lists
working_board_closed_lists = working_board.closed_lists()
read_list(working_board_closed_lists)
"""

#   Get and print open lists
working_board_open_lists = working_board.open_lists()
read_list(working_board_open_lists)

working_board_cards = working_board.all_cards()
#   read_list(working_board_cards)

list_to_add = 'Day'
list_to_search = f'<List {list_to_add}>'
print(f'Checking if Trello list {list_to_add} exists in current working board.')
if not(check_for_trello_list('<List Day>', working_board_open_lists)):
    print(f'Trello list {list_to_add} does not exist in current working board.\nCreating it.\n')
    working_board.add_list('Day', pos='bottom')
else:
    print(f'Trello list {list_to_add} exists.')



#       Get the cards in the weekly plan board
#   weekly_plan_board_cards = weekly_plan_board_class.all_cards()

#       Get the specific card you want to mess with
#   challenge = gtd_board_cards[1]


#       To add a checklist of 100 items in a card in a list in a board in trello,
#       you have to use something like the following.
#   challenge.add_checklist('Days', (f'Day {i:03d}' for i in range(1, 101)))


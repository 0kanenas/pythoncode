import random


class Card(object):
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit

    def show(self):
        print(f"{self.value} of {self.suit}")


class Deck(object):
    def __init__(self):
        self.cards = []
        self.build()

    def build(self):
        for suit in ["Spades", "Clubs", "Diamonds", "Hearts"]:
            for value in ["Ace"]+[i for i in range(2, 11)]+["Jack", "Queen", "King"]:
                self.cards.append(Card(value, suit))

    def show(self):
        for card in self.cards:
            card.show()

    def shuffle(self):
        return random.shuffle(self.cards)

    def draw_card(self):
        return self.cards.pop()


class Player(object):
    def __init__(self, name):
        self.name = name
        self.hand = []
        self.is_lucky: bool

    def draw(self, deck):
        self.hand.append(deck.draw_card())

    def show_hand(self):
        for card in self.hand:
            print(f"{self.hand.index(card)}\t", end='')
            card.show()

    def show_name(self):
        print(self.name)



#   Import modules
import random


#   Enter the number of players
number_of_players = int(input('Enter the number of players:\t'))
if number_of_players > 5:
    player_hand_size = 5
else:
    player_hand_size = 7

#   Card class


class Card(object):
    def __init__(self, card_value, card_suit):
        self.card_value = card_value
        self.card_suit = card_suit

    #   Show the card on the terminal
    def ShowCard(self):
        print(f"{self.card_value} of {self.card_suit}")

    #   Return the card value
    def CardValue(self):
        return self.card_value

#   Deck class


class Deck(object):
    def __init__(self):
        self.cards = []
        self.MakeDeck()

    #   Make the deck
    def MakeDeck(self):
        card_values = [
            "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"
        ]
        card_suits = [
            "Spades", "Diamonds", "Hearts", "Clubs"
        ]
        for card_suit in card_suits:
            for card_value in card_values:
                self.cards.append(Card(card_value, card_suit))

    #   Show the deck
    def ShowDeck(self):
        for card in self.cards:
            card.ShowCard()

    #   Return the deck length
    def DeckLength(self):
        return len(self.cards)

    #   Shuffle the deck
    def ShuffleDeck(self):
        random.shuffle(self.cards)

#   Player class


class Player(object):
    def __init__(self):
        self.name = self.GetPlayerName()
        self.hand_size = player_hand_size
        self.hand = Hand()

    #   Return the player name
    def PlayerName(self):
        return self.name

    #   Return the player hand size
    def HandSize(self):
        return self.hand_size

    #   Enter the player name
    def GetPlayerName(self):
        return str(input('Enter player\'s name:\t'))

#   Players class


class Players(object):
    def __init__(self):
        self.list_of_players = []
        self.MakePlayers()

    #   Make the players
    def MakePlayers(self):
        player_index = 0
        while player_index < number_of_players:
            self.list_of_players.append(Player())
            player_index += 1

    #   Make the players' hands
    def MakePlayersHands(self):
        card_index = 0
        for card in range(0, player_hand_size):
            for player in self.list_of_players:
                player.hand.MakeHand(ocean.cards[card_index])
                card_index += 1

    #   Show each player hand
    def ShowEachPlayerHand(self):
        for player in self.list_of_players:
            print(player.PlayerName())
            player.hand.ShowHand()


#   Hand class
class Hand(object):
    def __init__(self):
        self.cards = []

    #   Show the hand
    def ShowHand(self):
        for card in self.cards:
            card.ShowCard()

    #   Make the hand
    def MakeHand(self, card):
        self.cards.append(card)

    def CountFrequencyOfCard(self, value_count):
        counter = 0
        for card in self.cards:
            if card.card_value == value_count:
                counter += 1
        return counter

    def CheckForBookInHand(self):
        for card in self.cards:
            this_card_value = card.card_value
            if self.CountFrequencyOfCard(card.card_value) == 1:
                return [card for card in self.cards if card.card_value == this_card_value]
            else:
                return None

    def BookInHand(self):
        check = self.CheckForBookInHand()
        if check is None:
            print("There is no book of any card.")
        else:
            print(f"There are books of")


#   Make the ocean
ocean = Deck()
#   Shuffle the ocean
ocean.ShuffleDeck()
#   deck.ShowDeck()
#   print(ocean.DeckLength())
players = Players()
players.MakePlayersHands()

#   for i in range(0, player_hand_size*number_of_players):
#       ocean.cards[i].ShowCard()

#   players.ShowEachPlayerHand()

print(players.list_of_players[0].hand.BookInHand())

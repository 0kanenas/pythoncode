import numpy as np
import matplotlib.pyplot as plt

xs = list(range(0, 52))
ys = np.log(xs)
y1s = np.exp(xs)
xys = xs

plt.plot(xs, xys)
plt.plot(xs, ys)
plt.ylim(xys[0], xys[-1])
plt.yticks(xys)
plt.xlim(xs[0], xs[-1])
plt.xticks(list(num for num in xs if num % 6 == 0))
plt.plot(xs, y1s)
plt.grid()
plt.tight_layout()
plt.show()
'''
Usage:
   This script acts as an alarm.
Author:
   0kanenas
Notes:
    The user must have VLC working with the terminal and
    set the directory of the files which will play as the
    alarms. The script will function as a timer.
Possible improvements:
    Make the script function like an actual alarm.
'''

#  Import modules
import os
import time


#   Define executeCommand function
def executeCommand(cmd):
    print(cmd)
    time.sleep(a_time)
    os.system(cmd)


#   Get the directory of the files
directory = str(input('What\'s the directory of the alarms?\n'))

#   Make the command
command = f'vlc {directory}'

#   Get the time to ring the alarms
h_time = int(input('How many hours to count down?\n'))
m_time = int(input('How many minutes to count down?\n'))
s_time = int(input('How many seconds to count down?\n'))

a_time = h_time * 60 * 60 + m_time * 60 + s_time

executeCommand(command)

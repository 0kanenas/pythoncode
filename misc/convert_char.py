print('text to number')
string = 'AB-C'
print(string)
new_string = ''
for character in string:
    if character == '-':
        new_character = '.'
    else:
        new_character = ord(character) - 64
    new_string = f'{new_string}{new_character}'
print(new_string)


print('number to text')
string = '12.3'
print(string)
new_string = ''
for character in string:
    if character == '.':
        new_character = '-'
    else:
        new_character = chr(int(character)+64)
    new_string = f'{new_string}{new_character}'
print(new_string)

multiplier = 1.5

print('Hours are calculated relative to a day.')

goal_hours = float(input('Give me the goal amount of productive time in minutes.\n'))
hours = float(input('Give me the amount of productive time in minutes.\n'))

goal_hours = goal_hours / 60
hours = hours / 60

reward_rate = float(10)
print(f'Time reward rate in minutes is set to {reward_rate}.')
reward_rate = reward_rate / 60

if hours > goal_hours:
    overtime_hours = hours - goal_hours
    overtime_reward = overtime_hours * reward_rate * multiplier
    gross_reward = goal_hours * reward_rate + overtime_reward
else:
    gross_reward = hours*reward_rate

gross_reward = gross_reward*60

print(f'Total time reward in minutes: {gross_reward}')

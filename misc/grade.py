#   Make lists to store results for each lesson
resultingGradeList = []
greekList = []
mathList = []
compList = []
econList = []

#   Set the minimum grade for each lesson
greek = 11.0
math = 4.0
comp = 8.0
econ = 10.0

#   Set the step to use in the while loops
i = 0.1

#   Calculate the resulting grade
while greek <= 14.0:
    while math <= 7.0:
        while comp <= 11.0:
            while econ <= 13.0:
                
                #   Calculate the resulting grade
                resultingGrade = greek*200+math*330+comp*200+econ*270
                
                #   Write data to lists
                resultingGradeList.append(resultingGrade)
                greekList.append(greek)
                mathList.append(math)
                compList.append(comp)
                econList.append(econ)
                
                econ=+i
            comp=+i
        math=+i
    greek=+i        

#   Output resulting grade list
print(resultingGradeList)

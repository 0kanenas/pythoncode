'''
Usage:
   This script opens a webpage after a user-defiened set period of time.
Author:
   0kanenas
Notes:
Possible improvements:
'''

#  Import modules
import os, time

#  Get URL of webpage
url = str(input('URL of webpage to open:\n'))
#  Get time of time to wait to launch webpage
hrs = int(input('Hours to wait before opening webpage:\n'))
mns = int(input('Minutes to wait before opening webpage:\n'))
scs = int(input('Seconds to wait before opening webpage:\n'))
time_to_wait = scs+mns*60+hrs*60*60

#  Wait time
print(f'\nWaiting {hrs} hours, {mns} minutes, {scs} seconds.\n')
time.sleep(time_to_wait)
#  Open webpage
print(f'Opening "{url}".\n')
os.system(f'start {url}')

#  Terminate script
term = input()

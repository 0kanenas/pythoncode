#   Import modules
import os


#   Function to get video's formats
def get_formats(temp_file):

    #   Open file with the video's formats
    with open(temp_file, "r") as video_formats:
        formats = video_formats.readlines()

    #   Create a list from the lines that refer to the avaliable audio formats of the video
    audio_lines = [
        line for line in formats if "audio only" in line and line != '']

    #   Create a list from the audio bitrates
    audio_bitrates = [audio_line.split()[6] for audio_line in audio_lines]

    #   Edit the audio bitrate list so that all items have the same length
    audio_bitrates = [audio_bitrate.zfill(
        len(audio_bitrates[-1])) for audio_bitrate in audio_bitrates]

    #   Take the max bitrate out of the list
    best_audio_bitrate = max(audio_bitrates)

    #   Select the best quality audio format
    for audio_line in audio_lines:
        if best_audio_bitrate in audio_line:
            best_audio_format_code = audio_line.split()[0]

    #   Create a list from the lines that refer to the avaliable video formats of the video
    video_lines = [
        line for line in formats if "audio only" not in line and line != '']

    video_lines = [video_line.split()
                   for video_line in video_lines if video_lines.index(video_line) > 3]

    video_lines = [
        video_line
        for video_line in video_lines
        if len(video_line[0]) >= 3
    ]

    video_resolutions = [
        video_line[2]
        for video_line in video_lines
        if video_line[2] <= "2560x1440"
    ]

    best_video_resolution = max(video_resolutions)

    video_bitrates = [
        video_line[4]
        for video_line in video_lines
    ]

    video_bitrates = [
        video_bitrate.zfill(len(video_bitrates[-1]))
        for video_bitrate in video_bitrates
    ]

    best_video_bitrate = max(video_bitrates)

    for video_line in video_lines:
        if best_video_resolution in video_line and best_video_resolution in video_line:
            best_video_format_code = video_line[0]

    return f"{best_video_format_code}+{best_audio_format_code}"


def download_video_from(video_url):
    video_id = video_url.split('/')[-1]
    video_formats_file = f"{video_id}.txt"

    print(f"youtube-dl {video_url} --list-formats")
    os.system(f"youtube-dl {video_url} --list-formats")

    print(f"youtube-dl {video_url} --list-formats > {video_formats_file}")
    os.system(f"youtube-dl {video_url} --list-formats > {video_formats_file}")

    format_codes = get_formats(video_formats_file)
    format_to_download = f"--format \"{format_codes}\" --merge-output-format mkv"

    options = f"--no-overwrites --continue --console-title"

    output_path = "--output \"~/Videos/YouTube/%(uploader)s/%(title)s.%(ext)s\""

    print(
        f"youtube-dl {video_url} {output_path} {format_to_download} {options}")
    os.system(
        f"youtube-dl {video_url} {output_path} {format_to_download} {options}")

    print(f"rm {video_formats_file}")
    os.system(f"rm {video_formats_file}")


#   Check if .txt file with video urls exists. If not, it asks for URLs and saves
#   them to the .txt file.
if "videos_to_download.txt" not in os.listdir():
    video_urls = []
    video_url = str(input("Enter URL:\t")) + '\n'
    while "https" in video_url:
        video_urls.append(video_url)
        video_url = str(input("Enter URL:\t")) + '\n'
    with open("videos_to_download.txt", 'w') as urls:
        for video_url in video_urls:
            urls.writelines(video_url)

#   Reads URLs from .txt file
with open("videos_to_download.txt", 'r') as urls:
    video_urls = urls.readlines()

#   Checks if the video url list is empty
if len(video_urls) == 0:
    video_url = str(input("Enter URL:\t")) + '\n'
    while "https" in video_url:
        video_urls.append(video_url)
        video_url = str(input("Enter URL:\t")) + '\n'
    with open("videos_to_download.txt", 'w') as urls:
        for video_url in video_urls:
            urls.writelines(video_url)

#   Makes a list of read URLs and stripes the new line character
video_urls = [
    video_url.rstrip('\r\n')
    for video_url in video_urls
    if video_url.rstrip('\r\n') != ''
]


#   Statistics
total_downloads = 0
successful_downloads = 0
unsuccessful_downloads = []

#   Iterate through URL list
for video_url in video_urls:
    try:
        download_video_from(video_url)
        successful_downloads = successful_downloads + 1
        print(
            f"Successful download:\t{video_url}\nPlace in list:\t{successful_downloads}\n\n")
    except Exception:
        unsuccessful_downloads.append(video_url)
        print(f"Unsuccessful download:\t{video_url}\nError:\t{Exception}\n\n")
    total_downloads = total_downloads+1

print(f"Total downloads:\t{total_downloads}")
successful_downloads_percentage = round(
    successful_downloads/total_downloads*100)
print(
    f"Successful downloads:\t{successful_downloads}, {successful_downloads_percentage}%")

with open("videos_to_download.txt", 'w') as urls:
    for unsuccessful_download in unsuccessful_downloads:
        urls.write(unsuccessful_download + '\n')

'''
Usage:
   This script asks to downlaod a video from YouTube.
Author:
   0kanenas
Notes:
Future improvements:
'''

#  Import modules
import os


#   Define executeCommand function
def executeCommand(cmd):
    print(cmd)
    os.system(cmd)


#  Set directory to download video.
output = '--output "~/Videos/YouTube/%(uploader)s/%(title)s.%(ext)s"'

#  Get the link of video.
link = str(input('Link of video to download.\n'))

#   Ask for any additional options
customOptions = str(input('Are there any custom options you would like to use?\n'))

#  List avaliable format options.
listAvaliableFormatOptions = f'youtube-dl {link} --list-formats'
executeCommand(listAvaliableFormatOptions)

#  Get format
getFormatToDownload = str(input('\nSelect format of video to download. (code_of_video+code_of_audio)\n'))
getFormatToDownload = f'--format "{getFormatToDownload}" --merge-output-format mkv'

#  Set miscellaneous options.
options = '--ignore-errors --no-warnings --no-overwrites --continue --console-title'

#  Command to execute to download video.
if customOptions == '':
    command = f'youtube-dl {output} {link} {getFormatToDownload} {options}'
else:
    command = f'youtube-dl {output} {link} {getFormatToDownload} {options} {customOptions}'
executeCommand(command)

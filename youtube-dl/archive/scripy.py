#   Import modules
from modules import *


#   Main
def main():
    try:
        choice_a = menu_a()
        if choice_a == '1':
            os.chdir('/home/kanenas/youtube-dl/videos')
            download('video')
        if choice_a == '2':
            os.chdir('/home/kanenas/youtube-dl/music')
            download('music')
        if choice_a == '3':
            os.chdir('/home/kanenas/youtube-dl/podcast_shows')
            download('podcast_show')
        if choice_a == '0':
            raise RuntimeError
        else:
            print('Wrong input.\nExiting')
            raise RuntimeError
    except RuntimeError:
        pass


#   Menu
def menu_a():
    option_1 = '1.\tDownload Videos'
    option_2 = '2.\tDownload Music'
    option_3 = '3.\tDownload Podcast Shows'
    option_0 = '0.\tExit'
    print(
        f'Choose from the options below:\n{option_1}\n{option_2}\n{option_3}\n{option_0}')
    choice = str(input('Your choice:\t'))
    return choice


#   Run
main()

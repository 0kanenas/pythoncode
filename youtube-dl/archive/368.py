'''
Usage:
   This script downloads Casey Neistat's 368 show.
Author:
   0kanenas
Notes:
   The script works, it downloads the newest Casey Neistat's videos
   until the date 120180406. Though, after it finishes downloading,
   it checks if the enirety of the rest of his videos matches the
   date criterion.
Possible improvements:
   Privent the check of videos older than the date 20180406.
'''  

#  Import modules
import os

#  Set youtube-dl options

#  Set directory to download the show.
output = f'--output "E:\\YouTube\\%(uploader)s\\368\\%(title)s.%(ext)s"'
#  The first episode of the show went live on 120180406.
date = '--dateafter 20180406'
#  Get the link of Casey Neistat's YouTube channel.
channel = 'https://www.youtube.com/user/caseyneistat'
#  Set miscellaneous options.
options = '--ignore-errors --no-warnings --no-overwrites --continue'

# Command to execute to download the show.
os.system(f'youtube-dl {output} {options} {date} {channel}')

'''
Usage:
   This script asks to downlaod a podcast from YouTube.
Author:
   0kanenas
Notes:
Possible improvements:
'''  

#  Import modules
import os

#  Set youtube-dl options

#  Set directory to download podcast.
output = f'--output "E:\\YouTube\\Podcasts\\%(playlist)s\\Episodes\\%(title)s.%(ext)s"'
#  Get link of podcast.
link = str(input('Link of podcast to download.\n'))
#  Set miscellaneous options.
options = '--ignore-errors --no-warnings --no-overwrites --continue'
#  Set format options
format_op = '--extract-audio --audio-format mp3 --audio-quality 320K --embed-thumbnail \
--format bestaudio'

# Command to execute to download playlist.
os.system(f'youtube-dl {output} {format_op} {options} {link}')

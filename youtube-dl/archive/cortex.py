'''
Usage:
   This script downloads Cortex podcast from YouTube.
Author:
   0kanenas
Notes:
Possible improvements:
'''  

#  Import modules
import os

#  Set youtube-dl options

#  Set directory to download Cortex.
output = f'--output "E:\\YouTube\\Podcasts\\%(playlist)s\\Episodes\\%(title)s.%(ext)s"'
#  Set link of Cortex YouTube channel.
link = 'https://www.youtube.com/playlist?list=PLQ71hYrPwym-zhfxysyktJ9ElysROOHED'
#  Set miscellaneous options.
options = '--ignore-errors --no-warnings --no-overwrites --continue'
#  Set format options
format_op = '--extract-audio --audio-format mp3 --audio-quality 320K --embed-thumbnail \
--format bestaudio'

# Command to download podcast.
os.system(f'youtube-dl {output} {format_op} {options} {link}')

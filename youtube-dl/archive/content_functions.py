#   Import modules
from modules import *


#   Save content's formats
def saved_formats(content_link):
    content_id = content_link.split('?v=')[-1]
    content_format_file = f'{content_id}.txt'
    print(f'youtube-dl {content_id} --list-formats > {content_format_file}')
    os.system(
        f'youtube-dl {content_id} --list-formats > {content_format_file}')
    return content_format_file


#   Get content's best formats
def content_formats(file):
    with open(file, "r") as content_formats:
        format_lines = content_formats.readlines()
    audio_format_code = get_audio_format(format_lines)
    video_format_code = get_video_format(format_lines)
    formats = f'{video_format_code}+{audio_format_code}'
    print(f'rm {file}')
    os.system(f'rm {file}')
    return formats


def get_video_format(format_lines):
    video_lines = [
        line for line in format_lines if "audio only" not in line and line != '']
    video_lines = [video_line.split()
                   for video_line in video_lines if video_lines.index(video_line) > 3]
    video_lines = [
        video_line for video_line in video_lines if len(video_line[0]) >= 3]
    #   video_resolutions = [video_line[2] for video_line in video_lines]
    #   best_video_resolution = max(video_resolutions)
    video_bitrates = [video_line[4] for video_line in video_lines]
    video_bitrates = [video_bitrate.zfill(
        len(video_bitrates[-1])) for video_bitrate in video_bitrates]
    best_video_bitrate = max(video_bitrates)
    for video_line in video_lines:
        if best_video_bitrate in video_line:
            best_video_format_code = video_line[0]
    return best_video_format_code


def get_audio_format(format_lines):
    audio_lines = [
        line for line in format_lines if "audio only" in line and line != '']
    audio_bitrates = [audio_line.split()[6] for audio_line in audio_lines]
    audio_bitrates = [audio_bitrate.zfill(
        len(audio_bitrates[-1])) for audio_bitrate in audio_bitrates]
    best_audio_bitrate = max(audio_bitrates)
    for audio_line in audio_lines:
        if best_audio_bitrate in audio_line:
            best_audio_format_code = audio_line.split()[0]
    return best_audio_format_code


def get_links():
    links = []
    print('Enter "end" to stop entering video links.')
    while True:
        url = str(input('Enter video link:\t'))
        if url == 'end':
            break
        elif url == '' or len(url) != 43:
            continue
        else:
            links.append(url)
    return links


def download(choice):
    links = get_links()
    if not(len(links) == 0 or links[0] == 'end'):
        total_downloads = len(links)
        successful_downloads = 0
        successful_file_removals = 0
        for link in links:
            format_file = saved_formats(link)
            content_format = content_formats(format_file)
            download_command = f'youtube-dl {link} '
            download_command += f'--output "%(uploader)s/%(title)s.%(ext)s" '
            download_command += f'--ignore-errors --mark-watched --geo-bypass '
            download_command += f'--no-overwrites --continue --console-title --yes-playlist'
            if choice == 'video':
                download_command += f'--format {content_format} --merge-output-format mkv '
            else:
                download_command += f'--format {content_format} --extract-audio '
                download_command += f'--audio-format flac --audio-quality 0 '
                
            print(f'Downloading {link}')
            try:
                print(f'Successfully downloaded {link}')
                successful_downloads += 1
            except:
                print(f'An error occured while downloading {link}')
            delete_format_file = f'rm {format_file}'
            try:
                print(f'Successfully removed {format_file}')
                successful_file_removals += 1
            except:
                print(f'An error occured while removing {format_file}')
        print(f'Total downloads:\t{total_downloads}')
        print(f'Successful downloads:\t{successful_downloads}')
        print(
            f'Unsuccessful downloads:\t{total_downloads-successful_downloads}')

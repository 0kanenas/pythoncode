#  Import modules
import os


videos_to_download = "videos_to_download.txt"
with open(videos_to_download, 'r',) as video:
    videos = video.readlines()
videos = [x.strip() for x in videos]


def downlaod_video(link):

    def executeCommand(cmd):
        print(cmd)
        os.system(cmd)

    output = '--output "~/Videos/YouTube/%(uploader)s/%(title)s.%(ext)s"'

    listAvaliableFormatOptions = f'youtube-dl {link} --list-formats'

    executeCommand(listAvaliableFormatOptions)

    getFormatToDownload = str(input('\nSelect format of video to download. \
    (code_of_video+code_of_audio)\n'))

    getFormatToDownload = f'--format "{getFormatToDownload}" --merge-output-format mkv'

    options = '--ignore-errors --no-warnings --no-overwrites --continue --console-title'

    command = f'youtube-dl {output} {link} {getFormatToDownload} \
    {options} {customOptions}'

    executeCommand(command)


for i in range(len(videos)):
    downlaod_video(video)

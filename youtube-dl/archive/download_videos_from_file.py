from pprint import pprint
import os, pprint

#   Function to open a file and read each line
#   in an element of a list.
def open_file(file_name):
    with open(file_name) as line:
        lines = line.readlines()
    lines = [x.strip() for x in lines]
    return lines

#   Set the file which contains the video urls
#   to download.
videos_to_download = "videos_to_download.txt"

#   Define the list links to have all the links
#   contained in previously mentioned file.
links = open_file(videos_to_download)

#   Print each link from the list of links.
for i in range(len(links)):
    print(links[i])

#   Print and output the youtube-dl command which
#   lists all available formats of a video to a
#   .txt file.
for i in range(len(links)):
    print(f"youtube-dl {links[i]} --list-formats \
> temp_video_format{i}.txt")
#       os.system(f"youtube-dl {links[i]} --list-formats \
#   > temp_video_format{i}.txt ")


formats = []
for i in range(len(links)):
    formats.append(open_file(f"temp_video_format{i}.txt"))

for i in range(len(formats)):
    print(f"{formats[i][i]}\n")



import os
import json
import subprocess
from pprint import pprint


def welcome_menu():
    print('Download content from YouTube\nby 0kanenas\n\n\
Choices:\n\t\
1 - Download Video\n\t\
2 - Download Music\n\t\
3 - Download Podcast\n\t\
0 - Exit\n')
    user_choice = str(input('Your choice:\n\t'))
    print()
    if user_choice not in str([1, 2, 3, 0]):
        print('Invalid input')
    else:
        return user_choice


def playlist_menu():
    print('\nPlaylist link entered.\n\nOptions:\n\t\
1 - Download all videos in playlist\n\t\
2 - Return to entering video links\n\t\
0 - Exit\n')
    user_choice = str(input('Your choice:\n\t'))
    print()
    if user_choice not in str([1, 2, 0]):
        print('Invalid input')
    else:
        return user_choice

#   Check for directory


def check_directory(target_directory):
    current_directory = os.getcwd()
    directory_contents = os.listdir()
    if target_directory not in directory_contents:
        subprocess.call(f'mkdir {target_directory}', shell=True)
    working_directory = os.path.join(current_directory, target_directory)
    current_directory = os.chdir(working_directory)
    current_directory = os.getcwd()


#   Download function
def download(content):
    print()
    links, playlist_title = get_links()
    print()
    for link in links:
        format_file = saved_formats(link)
        content_format = content_formats(format_file, content)
        download_command = f'youtube-dl {link} --no-overwrites --continue --console-title \
--yes-playlist --ignore-errors --mark-watched --geo-bypass '
        if playlist_title == None:
            download_command = f'{download_command} --output "%(uploader)s - %(title)s.%(ext)s" '
        else:
            download_command = f'{download_command} --output "{playlist_title}/%(uploader)s - %(title)s.%(ext)s" '
        if content == '1':
            download_command = f'{download_command} --format {content_format} --merge-output-format mkv '
        else:
            download_command = f'{download_command} --format {content_format} --extract-audio '
            download_command = f'{download_command} --audio-format flac --audio-quality 0 '
        print(f'\nDownload {links.index(link)+1} of {len(links)}\n')
        try:
            subprocess.call(download_command, shell=True)
        except:
            print(f'Something went wrong while downloading {link}')
        print()


#   Get content links
def get_links():
    links = []
    print('Enter "end" to stop entering video links.')
    while True:
        link = str(input('Enter video link:\t'))
        playlist_title = None
        #   Input checks
        if link == 'end':  # If in input is end, break the loop
            break
        else:
            if link == '':  # If empty input, ask again for input
                print('No input.')
                continue
            else:
                if 'list' in link:  # If playlist in input, print message
                    playlist_choice = playlist_menu()
                    if playlist_choice != None:
                        if playlist_choice == '0':
                            try:
                                raise RuntimeError
                            except RuntimeError:
                                print('Exiting')
                                break
                        else:
                            if playlist_choice == '1':
                                #   DO STUFF
                                ids, playlist_title = work_with_playlist(
                                    link)
                                links.extend(make_full_youtube_link(ids))
                                break
                else:
                    links.append(link)
    return links, playlist_title


# make full youtube link
def make_full_youtube_link(ids):
    links = []
    for content_id in ids:
        if len(content_id) == 43:
            pass
        elif len(content_id) == 28:
            video_id = content_id.split('.be')[-1]
            link = f'https://www.youtube.com/watch?v={video_id}'
        elif len(content_id) == 11:
            link = f'https://www.youtube.com/watch?v={content_id}'
        links.append(link)
    return links


#   Save content's format
def saved_formats(content_link):
    content_id = content_link.split('?v=')[-1]
    content_format_file = f'{content_id}.txt'
    print(f'youtube-dl {content_id} --list-formats > {content_format_file}')
    subprocess.call(
        f'youtube-dl {content_id} --list-formats > {content_format_file}', shell=True)
    return content_format_file


#   Get content's format
def content_formats(file, content):
    with open(file, "r") as content_formats:
        format_lines = content_formats.readlines()
    audio_format_code = get_audio_format(format_lines)
    video_format_code = get_video_format(format_lines)
    if content == '1':
        formats = f'{video_format_code}+{audio_format_code}'
    else:
        formats = f'{audio_format_code}'
    subprocess.call(f'rm {file}', shell=True)
    return formats


#   Get content's video format
def get_video_format(format_lines):
    video_lines = [
        line for line in format_lines if "audio only" not in line and line != '']
    video_lines = [video_line.split()
                   for video_line in video_lines if video_lines.index(video_line) > 3]
    video_lines = [
        video_line for video_line in video_lines if len(video_line[0]) >= 3]
    #   video_resolutions = [video_line[2] for video_line in video_lines]
    #   best_video_resolution = max(video_resolutions)
    video_bitrates = [video_line[4] for video_line in video_lines]
    video_bitrates = [video_bitrate.zfill(
        len(video_bitrates[-1])) for video_bitrate in video_bitrates]
    best_video_bitrate = max(video_bitrates)
    for video_line in video_lines:
        if best_video_bitrate in video_line:
            best_video_format_code = video_line[0]
    return best_video_format_code


#   Get content's audio format
def get_audio_format(format_lines):
    audio_lines = [
        line for line in format_lines if "audio only" in line and line != '']
    audio_bitrates = [audio_line.split()[6] for audio_line in audio_lines]
    audio_bitrates = [audio_bitrate.zfill(
        len(audio_bitrates[-1])) for audio_bitrate in audio_bitrates]
    best_audio_bitrate = max(audio_bitrates)
    for audio_line in audio_lines:
        if best_audio_bitrate in audio_line:
            best_audio_format_code = audio_line.split()[0]
    return best_audio_format_code


def work_with_playlist(link):
    print('Getting playlist information.')
    subprocess.call(
        f'youtube-dl --flat-playlist --dump-single-json {link} > playlist_info_signle.json', shell=True)
    with open('playlist_info_signle.json', 'r') as playlist_info:
        lines = playlist_info.readlines()
    subprocess.call(f'rm playlist_info_signle.json', shell=True)
    keys = []
    for line in lines:
        key = line.split(' ')
        keys.extend(key)
    keys.reverse()
    playlist_title_index = keys.index('"title":')
    playlist_title_index = playlist_title_index - 1
    playlist_title = keys[playlist_title_index]
    playlist_title = playlist_title[1:-2]
    print(f'\n{playlist_title}\n')
    keys.reverse()
    ids = []
    key_index = 0
    for key_index in range(len(keys)):
        if keys[key_index] == '"id":':
            ids.append(keys[key_index+1][1:-2])
    ids.pop()
    return ids, playlist_title


#   Main function


def main():
    content_choice = welcome_menu()
    if content_choice != None:
        if content_choice == '0':
            try:
                raise RuntimeError
            except RuntimeError:
                print('Exiting')
        else:
            check_directory('youtube-dl')
            if content_choice == '1':
                print('Downloading Video')
                check_directory('Video')
            else:
                if content_choice == '2':
                    print('Downloading Music')
                    check_directory('Music')
                else:
                    print('Downloading Podcast')
                    check_directory('Podcast')
            download(content_choice)


main()
